# Deploy Let's Chat on Clever Cloud

- This project create and deploy **[Let's Chat project](https://sdelements.github.io/lets-chat/)** on **[Clever Cloud](https://www.clever-cloud.com/)** from a GitLab instance (self hosted or GitLab.com)

## How to use it?

### First

- Fork or clone this project
- Push the fork or the clone to GitLab.com or your own GitLab instance

### Add secret variables

Go to **Settings > CI/CD > Variables** and add this variables:

- `APP_NAME`, the name of your runner
- `ORGANIZATION_NAME`, the name of the organization (on Clever Cloud) where you are deploying the runner
- `REGION`, the region of the deployment (eg: `par` for Paris)
- `ADDON_NAME`, the name of the MongoDB Addon
- `ADDON_REGION`, the region of the deployment (eg: `eu` for Paris)
- `CLEVER_SECRET`, this value is used for the authentication on Clever Cloud
- `CLEVER_TOKEN`, this value is used for the authentication on Clever Cloud

> :wave: *Remark*: Since GitLab 10.8 you can override these values by specifying variables when using manual pipelines.

### Generate a "Let's Chat Server"

- Go to the **CI/CD > Pipelines** section
- Click on the button **Run Pipeline**
- If needed, override the values
- Click on the button **Create Pipeline**
- You'll get 1 manual job:
  - **create** from the `🚀create_chat_server` stage

Click on **create** and in some moments you'll obtain a Let's Chat Server.





